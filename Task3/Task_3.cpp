// Task3.cpp 
// �140353 11

#include "stdafx.h" 
#include "Task_3.h" 
#define _USE_MATH_DEFINES // for C++
#include <cmath>
#include <iostream>
#include <string>

using namespace std;

Calculator::Calculator(double _x, double _y, double _z) :x(new double(_x)), y(new double(_y)), z(new double(_z)){};

Calculator::~Calculator() {
	delete x,y,z;
	cout << "...destructor has been called" << endl;
}

void run(Calculator& c){
	c.result = pow(2, pow(*c.y, *c.x)) + pow(pow(3, *c.x), *c.y) - (*c.y*(atan(*c.z) - M_PI / 6)) / (fabs(*c.x) + (1 / (pow(*c.y, 2) + 1)));
}

void print(Calculator& c){
	cout << "���������(4.25): c = " << c.result << endl;
}



int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");
	cout << "��������� �������� �� �������." << endl << endl;

	Calculator calculator(3.251, 0.325, 0.466e-4);
	run(calculator);
	print(calculator);

	cout << endl;
	system("pause");
	return 0;
}