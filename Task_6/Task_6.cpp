// Task6.cpp 
// �140353 11
// 11.	(a1+a2+a3), (a2+a3+a4), (a3+a4+a5), � (aN-2+aN-1+aN);

#include "stdafx.h"
#include <iostream>

using namespace std;

template <typename T> class arrayTemplate
{
private:
	int size;
	int counter;
	T* array;
	T tmp = NULL;
	T* arrayResult;
public:
	arrayTemplate(int n);
	~arrayTemplate();
	void add(T element);
	void print();
};

template <typename T>  arrayTemplate<T>::arrayTemplate(int _size)
{
	size = _size;
	counter = 0;
	array = new T[size];
	arrayResult = new T[size-2];
}

template <typename T>  arrayTemplate<T>::~arrayTemplate()
{
	delete array;
}

template <typename T> void arrayTemplate<T>::add(T element)
{
	if (counter > size) {
		cout << "����� �� ������� �������!" << endl;
		return;
	}
	tmp += element;
	array[counter] = element;
	//cout << "counter: "<< counter << endl;
	if (counter>1) {
		arrayResult[counter-2] = array[counter] + array[counter - 1] + array[counter-2];
		//cout << "arrayResult[" << (counter) << "] = " << "arrayResult[" << (counter) << "] + " << "arrayResult[" << (counter - 1) << "] + " << "arrayResult[" << (counter - 2) << "] " << endl;
		tmp = NULL;
	}
	counter++;
}

template <typename T> void arrayTemplate<T>::print()
{

	for (int i = 0; i < size; i++){
		cout << array[i] << " ";
	}
	cout << endl << "------------------------" << endl;
	for (int i = 0; i < size - 2; i++){
		cout << arrayResult[i] << " ";
	}
	cout << endl << "========================" << endl;	
	cout << endl;
}

class El{
	int x;
public:
	El(int _x){ x = _x; }
};

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");

	arrayTemplate<int> intArray(8);
	intArray.add(9);
	intArray.add(8);
	intArray.add(7);
	intArray.add(6);
	intArray.add(5);
	intArray.add(4);
	intArray.add(3);
	intArray.add(2);
	intArray.print();

	arrayTemplate<float> floatArray(6);
	floatArray.add(1.2);
	floatArray.add(2.1);
	floatArray.add(1.11);
	floatArray.add(2.31);
	floatArray.add(3.11);
	floatArray.add(4.11);
	floatArray.print();

	arrayTemplate<char> charArray(9);
	charArray.add('a');
	charArray.add('b');
	charArray.add('c');
	charArray.add('d');
	charArray.add('e');
	charArray.add('f');
	charArray.add('1');
	charArray.add('2');
	charArray.add('3');
	charArray.print();

	for (int i = 0; i < 15; i++){
		//cout << i << " " << (i % 3) << " " << (i / 3) << endl;
	}

	cout << endl;
	system("pause");
	return 0;
}

