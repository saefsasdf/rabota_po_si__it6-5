// Task_2.cpp  
// �140353 11

#include "stdafx.h" 
#include "Task_2.h" 
#define _USE_MATH_DEFINES // for C++
#include <cmath>
#include <iostream>
#include <string>

using namespace std;

void Calculator::set(double x, double y, double z){
	this->x = x;
	this->y = y;
	this->z = z;
}
void Calculator::run(){
	if (fabs(x) == -(1 / (pow(y, 2) + 1))) {
		cout << "����������� ��������!";
		return;
	}
	this->result = pow(2, pow(y, x)) + pow(pow(3, x), y) - (y*(atan(z) - M_PI / 6)) / (fabs(x) + (1 / (pow(y, 2) + 1)));
}
void Calculator::print(){
	cout << "���������(4.25): c = " << this->result << endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");

	cout << "��������� �������� �� �������." << endl << endl;

	Calculator calculator;
	Calculator *pCalculator = &calculator;
	calculator.set(3.251, 0.325, 0.466e-4);
	pCalculator->run();
	(&calculator)->print();

	Calculator *pCalculator2 = new Calculator;
	pCalculator2->set(3.251, 0.325, 0.466e-4);
	pCalculator2->run();
	(*(pCalculator2)).print();

	delete pCalculator2;

	cout << endl;
	system("pause");
	return 0;
}