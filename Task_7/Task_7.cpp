// Task7.cpp 
// �140353 11
// 8. ����� ������  (���, �����, ���������, ���� ������ �� ������, 
// ���������� ���� - ������� Run(), ������)

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <iomanip>

using namespace std;

class AbstractBaseClass{
public:
	AbstractBaseClass(){};
	virtual ~AbstractBaseClass(){ cout << "���������� AbstractBaseClass."; };
	virtual void print() = 0;
	friend void run();
};

class FIO{
public:
	string fio;
	FIO() : fio(){};
	FIO(string _fio) :fio(_fio){};
	~FIO(){ cout << "���������� FIO."; };
};

class Info{
public:
	string department, position;
	time_t  hiringDate;
	int salary = 0;
	Info(){};
	Info(string _department, string _position, int _salary, int year, int month, int day) :department(_department), position(_position), salary(_salary){
		hiringDate = mktime(&make_tm(year, month, day));
	};
	~Info(){ cout << "���������� Info."; };

	tm make_tm(int year, int month, int day)
	{
		std::tm tm = { 0 };
		tm.tm_year = year - 1900; // years count from 1900
		tm.tm_mon = month - 1;    // months count from January=0
		tm.tm_mday = day;         // days count from 1
		return tm;
	}
};

class Employee : public AbstractBaseClass, public FIO, public Info{
	char buffer[80];
	int portable_difference;
	time_t now = time(NULL);
	char* format = "%A, %B %d, %Y %I:%M:%S";
public:
	Employee(){};
	Employee(string _fio, string _department, string _position, int _salary, int year, int month, int day) :FIO(_fio), Info(_department, _position, _salary, year, month, day){
		strftime(buffer, 80, format, localtime(&hiringDate));
		portable_difference = std::difftime(now, hiringDate) / (60 * 60 * 24);
	}
	~Employee(){ cout << "���������� Employee."; };

	void print(){
		cout << "---------------------------------------------" << endl;
		cout << "���: " << fio << endl;
		cout << "�����: " << department << endl;
		cout << "���������: " << position << endl;
		strftime(buffer, 80, format, localtime(&hiringDate));
		cout << "���� ������ �� ������: " << buffer << endl;
		cout << "---------------------------------------------" << endl;
	};

	void run(){
		time_t seconds_now = time(NULL);
		time_t now = std::mktime(localtime(&seconds_now));
		cout << "���������� ���� : " << portable_difference << " ��." << endl;
	}
	friend void saveinfile(Employee& e)
	{
		try{
			ofstream f;
			f.open("text.txt", ios_base::app);
			f << e.FIO::fio << ", " << e.department << ", " << e.position << ", " << e.salary << " ���. , " << e.buffer << ", " << e.portable_difference << " ��. \n";
			f.close();
		}
		catch (...)
		{
			cout << "������ ��� ������ � ����." << endl;
		}
	}
};

int _tmain(int argc, _TCHAR* argv[]){

	setlocale(LC_ALL, "Russian");

	Employee e = Employee("������ ���� ��������", "IT", "Senior C++ Developer", 30000, 2016, 1, 16);
	e.print();
	e.run();
	saveinfile(e);

	e = Employee("������ ���� ��������", "IT", "Junior C++ Developer", 40000, 2015, 12, 21);
	e.print();
	e.run();
	saveinfile(e);

	Employee *pE = &e;
	pE->print();
	
	Employee *pE2 = new Employee("������� ����� ���������", "IT", "Senior C++ Developer", 60000, 2016, 1, 10);
	pE2->print();	
	pE2->run();
	delete pE2;

	cout << endl;
	system("pause");
	return 0;
}















