// Task_1.cpp 
// �140353 3, 28

#include "stdafx.h" 
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

void fillArray(int* m, int size);
void printArray(int* m, int size);
int minimumOfArray(int* m, int size);
void addToArray(int* m, int size, int term);

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");

	int lengthA, lengthB, lengthC, minA, minB;
	cout << "3. ������ ��� ������� �(5) � �(5). � ������ �� �������� ����� ����������" << endl << " �������� � ��������� ��� �� ���� ��������� ��������. �� ������ �������" << endl << "�������� � ��������������� �������." << endl << endl;
	cout << "������� ������ ������� A: ";
	cin >> lengthA;
	cout << "������� ������ ������� B: ";
	cin >> lengthB;

	int *arrayA = new int[lengthA];
	int *arrayB = new int[lengthB];

	cout << endl << "��������� ������ A:" << endl;
	fillArray(arrayA, lengthA);

	cout << endl << "��������� ������ B:" << endl;
	fillArray(arrayB, lengthB);

	cout << "----------------------" << endl;

	cout << "�������� ������ A:" << endl;
	printArray(arrayA, lengthA);

	cout << endl << "�������� ������ B:" << endl;
	printArray(arrayB, lengthB);

	minA = minimumOfArray(arrayA, lengthA);
	minB = minimumOfArray(arrayB, lengthB);

	cout << endl << "������� � ������� A: " << minA << endl;
	cout << "������� � ������� B: " << minB << endl;

	addToArray(arrayA, lengthA, minA);
	addToArray(arrayB, lengthB, minB);

	cout << "----------------------" << endl;
	cout << "������ A ����� ����������� ������������(" << minA << "):" << endl;
	printArray(arrayA, lengthA);
	cout << endl << "������ B ����� ����������� ������������(" << minB << "):" << endl;
	printArray(arrayB, lengthB);

	delete[] arrayA; delete[] arrayB;

	cout << "-------------------------------------------------------------------------------" << endl;
	cout << endl << "28. ��� ������ A(n,n). ��������� ����� ���� ��������������� ���������, � �����" << endl << " �� ����������." << endl << endl;

	cout << "������� ������ ������� A(n,n): ";
	cin >> lengthC;


	int **arrayC = new int*[lengthC];
	for (int i = 0; i < lengthC; i++) arrayC[i] = new int[lengthC];
	
	cout << endl << "��������� ������ A(" << lengthC << " x " << lengthC << "):" << endl;	
	for (int i = 0; i < lengthC; i++)
	{
		for (int j = 0; j < lengthC; j++)
		{
			cout << "������� �������� Array[" << i << ", " << j << "]: ";
			cin >> *(*(arrayC + i) + j);
		}
	}

	int positiveCount = 0, positiveSum = 0;
	cout << endl << "�������� ������ A(" << lengthC << " x " << lengthC << "):" << endl;
	for (int i = 0; i < lengthC; i++)
	{
		for (int j = 0; j < lengthC; j++)
		{
			cout << "A[" << i << ", " << j << "] = " << *(*(arrayC + i) + j) << endl;
			if (*(*(arrayC + i) + j) >= 0) {
				positiveCount ++;
				positiveSum += *(*(arrayC + i) + j);
			}
		}
	}

	cout << endl << "����� ���� ��������������� ��������� = " << positiveSum << endl;
	cout << endl << "���������� ���� ��������������� ��������� = " << positiveCount << endl;

	delete[] arrayC;

	cout << endl;
	system("pause");
	return 0;
}

void fillArray(int* m, int size){
	for (int i = 0; i < size; i++){
		cout << "������� �������� Array[" << i << "]: ";
		cin >> *(m + i);
	}
}

void printArray(int* m, int size){
	for (int i = 0; i < size; i++){
		cout << " Array[" << i << "] = " << *(m + i) << endl;
	}
}

int minimumOfArray(int* m, int size){
	int min = *(m);
	for (int i = 1; i < size; i++){
		if (*(m + i) < min) min = *(m + i);
	}
	return min;
}

void addToArray(int* m, int size, int term){
	for (int i = 0; i < size; i++){
		*(m + i) += term;
	}
}

