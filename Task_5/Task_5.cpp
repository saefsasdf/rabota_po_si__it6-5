// Task5.cpp 
// �140353 11
// 11.	�������� �1*�2/�

#include "stdafx.h"
#include <iostream>

using namespace std;

class X{

public:
	int *x1, *x2;
	X() : x1(new int(5)), x2(new int(6)){cout << "����������� �������� ������ �� ���������(x1 = 5, x2 = 6)."<< endl;}
	X(int _x1, int _x2) : x1(new int(_x1)), x2(new int(_x2)){ cout << "����������� �������� ������ � �����������(x1 = " << _x1 << ", x2 = " << _x2 << ")." << endl; }
	~X(){
		cout << "���������� �������� ������." << endl;
		delete x1, x2;
	}

	virtual void showX1(){ cout << "����� �������� ������ showX1(), x1 = " << *(x1) << endl; }
	virtual void showX2(){ cout << "����� �������� ������ showX2(), x2 = " << *(x2) << endl; }
	virtual void setX1(int _x1){ cout << "����� �������� ������ setX1(" << _x1 << ")" << endl; *(x1) = _x1; }
	virtual void setX2(int _x2){ cout << "����� �������� ������ setX2(" << _x2 << ")" << endl; *(x2) = _x2; }
};

class Y : public X{
	int *y;
public:
	Y() :X(){
		y = new int(3);
	}
	Y(int x1, int x2, int _y) :X(x1, x2)
	{
		y = new int(_y);
	}
	~Y(){
		cout << "���������� ������������ ������." << endl;
		delete y;
	}
	void showY(){ cout << "y = " << *(y) << endl; }
	void setY(int _y){ *(y) = _y; }
	void showX1(){ cout << "����� ������������ ������ showX1(), x1 = " << *(x1) << endl; }
	void showX2(){ cout << "����� ������������ ������ showX2(), x2 = " << *(x2) << endl; }
	void setX1(int _x1){ cout << "����� ������������ ������ setX1(" << _x1 << ")" << endl; *(x1) = _x1; }
	void setX2(int _x2){ cout << "����� ������������ ������ setX2(" << _x2 << ")" << endl; *(x2) = _x2; }
	void run(){ 
		cout << *(x1) << " * " << (*(x2)) << " / " << (*(y)) << " = " << *(x1)*(*(x2)) / (*(y)) << endl;
		cout << "��������� �1 * �2 / � = " << *(x1)*(*(x2)) / (*(y)) << endl; }
};

int _tmain(int argc, _TCHAR* argv[])
{

	setlocale(LC_ALL, "Russian");
	X *x = new X();
	Y y = Y();


	x->showX1();
	x->showX2();
	y.showY();
	y.run();

	x = &y;

	x = new Y(9, 4, 6);
	((Y*)x)->showX1();
	((Y*)x)->showX2();

	x->setX1(10);
	x->X::setX2(20);
	((Y*)x)->setY(4);
	((Y*)x)->run();

	cout << "����������� ����."<<endl;
	((X*)x)->showX1();
	delete x;

	Y *y2 = new Y();
	delete y2;	

	cout << endl;
	system("pause");
	return 0;
}

