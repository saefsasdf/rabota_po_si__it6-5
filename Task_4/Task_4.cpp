// Task4.cpp 
// �140353 24
// 24.	����� L ������, �� ���������� ��������� �� ���������� �������
// 21.	����� L ������ 3, �� ��������� ������ 3-� ������
#include "stdafx.h" 
#include "Task_4.h" 
#define SIZE 255 //����� ������ �� ���������
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <istream>

String::String(){
	str = new char[SIZE];
	str[0] = '\0';
	str_return = new char[SIZE];
	str_return[0] = '\0';
};
String::String(char *s){
	str = new char[SIZE];
	strcpy(str, s);
	str_return = new char[SIZE];
	str_return[0] = '\0';
};
String::~String(){
	delete[] str;
	std::cout << "...destructor has been called" << endl;
};
void String::Set(char* s){
	unsigned int i;
	for (i = 0; i < strlen(s); i++)
		str[i] = s[i];
	str[i] = '\0';
};
char* String::Run(){ /*�����, �������� ���������� ������, � ������ ������ - ���� ����� L ������, �� ���������� ��������� �� ���������� �������*/
	int j = 0;
	if (!((strlen(str) + 1) % 3 == 0)) {
		for (int i = 0; i < strlen(str); i++) {
			if (((i+1) % 3) != 0) {
				str_return[j] = str[i];
				j++;
			}
		}
		str_return[j] = '\0';
	}
	else strcpy(str_return, str);

	return str_return;
};
char* Run(String &obj){ return obj.Run(); };
void print(String &obj){ cout << obj.str << " " << obj.str_return << endl; };

ostream& operator<<(ostream &stream, String &ob) {
	cout << " ���������� ��������� '<<' " << endl;
	stream << ob.str;
	return stream;
};
istream &operator>>(istream &stream, String &ob){
	cout << " ���������� ��������� '>>' " << endl;
	stream >> ob.str;
	return stream;
};
String& String::operator=(String &str_in) //���������� ��������� ������������
{
	if (this == &str_in)	{ return *this; }
	strcpy(str, str_in.str);
	strcpy(str_return, str_in.str_return);
	return *this;
}

void String::saveInFile(string s)
{
	try{
		ofstream f;
		f.open("text.txt", ios_base::app);
		f << "�������� ������:        " << s << " \n";
		f.close();
	}
	catch (...)
	{
		cout << "������ ��� ������ � ����." << endl;
	}
}

void String::saveInFile()
{
	try{
		ofstream f;
		f.open("text.txt", ios_base::app);
		f << "��������������� ������: " << str_return << " \n";
		f.close();
	}
	catch (...)
	{
		cout << "������ ��� ������ � ����." << endl;
	}
}

int _tmain(int argc, _TCHAR* argv[]){

	setlocale(LC_ALL, "Russian");

	char s[256];

	cout << "Type anything and press \"Enter\":" << endl;
	cin.getline(s, 256); //��������� ��������� ��� ������
	String str(s); //������ � ������� ������ ��������������� ����� ����������,
	//��������� �������� ������������� ����� �����������
	cout << endl << "You have type:" << endl;
	print(str);
	cout << endl << "Output string:" << endl;
	cout << Run(str) << endl;
	str.saveInFile(s);
	str.saveInFile();
	cout << "-----------------------" << endl << endl;

	cout << "Type anything and press \"Enter\":" << endl;
	cin.getline(s, 256);
	String *pstr; //������ � ������� ������ ����� ���������
	pstr = new String();
	pstr->Set(s);
	cout << endl << "You have type:" << endl;
	print(*pstr);
	cout << endl << "Output string:" << endl;
	cout << Run(*pstr) << endl;

	cout << endl << *(pstr) << endl << endl;

	pstr->saveInFile(s);
	pstr->saveInFile();

	String first("������");
	String second("������");
	cout << endl << first << endl << endl;
	cout << endl << second << endl << endl;
	first = second;
	cout << endl << first << endl << endl;

	cout << "Type anything and press \"Enter\":" << endl;
	cin >> second;
	cout << endl << second << endl << endl;

	delete pstr;

	cout << endl;
	system("pause");
	return 0;
};


