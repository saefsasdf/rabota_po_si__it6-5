//������������ ������ 4 "����� ������������ ������ � ���������� ��������"
//�������:�������� � ��������� ���������� � ���������� ��������.

//4.(20 �������) 20.����� L ��������, �� ��������� ��������� �� ������� �������
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include  <cstdlib>
#include <time.h>
#include <iomanip>//����������� ��� ���������������� ������ ����� cout
#include <math.h>
#include <iostream>
#include <string.h>

#define SIZE 255//������ ������ �� ��������� 

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
using namespace std;

class Cstroka//����� ������
{
private:
	char *stroka;//������ ��������
	char *resfun;//�������������� ������(������ ���������� � ���� ���������� ������� �� ��������)

public:
	Cstroka();//����������� ��� ����������
	Cstroka(char*s);//����������� � �����������
	~Cstroka();//���������� ������
	void set(char*s);//����� ����� �������� ������
	char* run();//����� ������� ������ �� ��������
	friend char* run(Cstroka&cs);//������������� ����� ������� ������������ ������ ������
	friend void print(Cstroka&cs);//������������� ����� ������ �� ������
	friend ostream& operator<<(ostream&, Cstroka&cs);//���������� ��������� ������
	friend istream& operator>>(istream&, Cstroka&cs);//���������� ��������� �����
	Cstroka& operator=(const Cstroka&cs);//���������� ��������� ������������
	friend void save(Cstroka& st);//������ � ����
};
//���������� ������� � ������� ��� ���� ������
Cstroka::Cstroka()//����������� ��� ����������
{
	stroka = new char[SIZE];//��������� ������ ��� ������
	stroka[0] = '\0';//��������� ������� ����� ������(������ �����)
	resfun = new char[SIZE];//��������� ������ ��� ������ ���������� ������� ������ �� ��������
	resfun[0] = '\0';//��������� ������� ����� ������(������ �����)
}
Cstroka::Cstroka(char*s)//����������� � ����������(�������� ������)
{
	stroka = new char[SIZE];
	strcpy(stroka, s);
	resfun = new char[SIZE];
	resfun[0] = '\0';
}
Cstroka::~Cstroka()//���������� ������.������������ ������ ���������� ��� ����������
{
	delete stroka;
	delete resfun;
	cout << "������ ������ Cstroka ������� ������" << endl;
}
void Cstroka::set(char*s)//����� ����� �������� ������(������)
{
	strcpy(stroka, s);
	resfun[0] = '\0';
}
char* Cstroka::run()//����� ������� ������ �� ��������
{
	//20.	����� L ��������, �� ��������� ��������� �� ������� �������
	unsigned int leng = strlen(stroka);
	cout << ":: " << leng << endl;
	int n = leng % 2;
	cout << ":: " << n << endl;

	if (n != 0)
	{
		int schet = 0;//������� �������� ������� ����������� ����������� � �������������� ������
		int probel = 0;//������� ��������(����� ������� ������� schet ������������� ������
		for (int i = 0; i<leng; i++)
		{
			if (stroka[i] == ' ') { probel++; }
			if (probel<2){ schet++; }
		}
		for (int i = 0; i<schet; i++)
			resfun[i] = stroka[i];
		resfun[schet] = '\0';
		return resfun;
	}
	return stroka;
}
char* run(Cstroka& cs)//������������� ����� ������� ������������ ������ ������
{
	return cs.run();
}
void print(Cstroka& cs)//������������� ����� ������ �� ������
{
	cout << cs.stroka << "  " << cs.resfun << endl;
}
ostream& operator<<(ostream&stream, Cstroka&cs)//���������� ��������� ������
{
	stream << cs.stroka;
	return stream;
}
istream& operator>>(istream&stream, Cstroka&cs)//���������� ��������� �����
{
	stream >> cs.stroka;
	return stream;
}
Cstroka& Cstroka::operator=(const Cstroka&cs)//���������� ��������� ������������
{
	//�������� �� ����������������
	if (this == &cs)	{ return *this; }
	strcpy(stroka, cs.stroka);
	strcpy(resfun, cs.resfun);
	return *this;
}

void save(Cstroka& st)//������ � ����
{
	ofstream f;
	f.open("text.txt", ios::app);
	f << "�������� ������: " << st.stroka << " �������������� ������: " << st.resfun;
	f.close();
}

int main(int argc, char** argv)
{
	setlocale(LC_ALL, "Russian");
	char s[255];
	cout << "������� ������,������� Enter ��������� ���� ������:" << endl;
	cin.getline(s, 256);//��������� ��������� ��� ������
	Cstroka string(s);//������ � ������� ������ ��������������� ����� ����������,��������� �������� ������������� ����� �����������
	cout << "����� ���������� ������:" << endl;
	print(string);//������ ����� ������ �� �������
	cout << "��������� ���������� ������� run" << endl;
	cout << run(string) << endl;
	save(string);
	cout << endl;
	//
	cout << "������� ������,������� Enter ��������� ���� ������:" << endl;
	cin.getline(s, 256);//��������� ��������� ��� ������	
	Cstroka *str = new Cstroka();//������ � ������� ������ ����� ���������. ������������� ������� ����� ����������� � ����������
	str->set(s);//��������� ������ ��������� � ������
	print(*str);//����� ������ �� ������� ����� ���������

	cout << "��������� ���������� ������� run" << endl;
	cout << run(*str) << endl;
	delete str;//�������� �������� ������ Cstroka
	//�������� ��������� ������������
	cout << endl;
	cout << "�������� �������������� ��������� ������������:" << endl;
	Cstroka str2; str2.set("Test 1234");
	cout << "������ ������ �������,�� �������� ������������:" << endl;
	print(str2);
	cout << "������ ������ �������,����� ������������:" << endl;
	str2 = string;
	print(str2);


	system("pause");
	return 0;
}
